---
# An instance of the Contact widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

title: Contact
subtitle:

content:
  # Contact (edit or remove options as required)

  email: zhengfan@shanghaitech.edu.cn
  address:
    street: 393 Huaxia Middle Road (Pudong new area)
    city: Shanghai
    postcode: '201210'
    country: China
    country_code: CN
  coordinates:
    latitude: '31.179016'
    longitude: '-238.411566'
  directions: SPST building 5-205.E

  #contact_links:
  #  - icon: comments
  #    icon_pack: fas
  #    name: Discuss on Forum
  #    link: 'https://discourse.gohugo.io'

  # Automatically link email and phone or display as text?
  autolink: true

  # Email form provider
#  form:
#    provider: netlify
#    formspree:
#      id:
#    netlify:
      # Enable CAPTCHA challenge to reduce spam?
#      captcha: false

design:
  columns: '1'
---

## **We are hiring!** 
New positions are available for postdocs and potential graduate students. If you are interested, send your CV to me (zhengfan@shanghaitech).

<!---<iframe src="https://restapi.amap.com/v3/staticmap?markers=-1,https://a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png,0:121.593434,31.176016&zoom=14&key=206b4d2a528a76d2c5edf35fb90d8994"width="600" height="450" frameborder="0" style="border:0"></iframe> --->
