---
title: MSE1701
summary: Here we describe how to add a page to your site.
date: "2022-08-08T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
header:
  caption: ""
  image: ""
---



### Time

Thu 9:10 am - 11:55 am 

### Homework and tutorials

[Course gitlab project](https://gitlab.com/zhengtheorygroup/mse1701-2024-spring)
<!---https://gitlab.com/zhengtheorygroup/mse1701--->


### Prerequisite	

Calculus, General physics, and Programming language

### Required coding language

python or C or Julia or Matlab or even Fortran (at least you know one language)

### Grading policy

50% Homework + 25% one exam + 25% final project

### Office hour

Friday 4:00pm - 5:30pm 

### Syllabus and notes

* **week 1: Introduction** <br/>Introduction to computational material science and course miscellaneous

* **week 2: Atomic interactions Introduction** <br/>covalent bond, ionic bond, bond length, bond angle, dihedral angle, Coulomb and weak atomic interaction

* **week 3: Force and structural relaxation** <br/> Force calculation and relaxation method
  
* **week 4: Mechanism of molecular dynamics (MD)** <br/> MD mechanism and method
  
* **week 5: Application of MD simulation** <br/> Correlation function and spectroscopy
  
* **week 6: MD simulation** <br/> Use LAMMPS to simulate water
  
* **week 7: MD simulation** <br/> Use LAMMPS to simulate metal and semiconductor
  
* **week 8: Introduction of electronic structure** <br/> Energy band 
  
* **week 9: First-principle calculation and density functional theory (DFT)** <br/> Introduction to first-principle calculation and DFT
  
* **week 10: Electronic structure and solar cell material** <br/> Use Quantum Espresso (QE) to compute energy band for semiconductor
  
* **week 11: Computational mechanics** <br/> Us QE to compute material mechanics
  
* **week 12: Structural relaxation and vibration of metal and low-dimensional material** <br/> Use QE to optimize structure and compute phonons
  
* **week 13: Structure and electronic structure of polymer; limitation of DFT I** <br/> Use QE to study low-diemensional materials and xc functionals
  
* **week 14: Surface and catalysis** <br/> Use QE to simulate metal surface and surface reaction
  
* **week 15: ab initio molecular dynamics; Material database; limitation of DFT II** <br/> Perform ab initio MD and methods beyond Born-Oppenheimer approximation
  
* **week 16: Monte-Carlo in computational materials** <br/> structure of alloy and growth of crystal by Monte-Carlo, genetic algorithm, and kinetic Monte-Carlo methods
