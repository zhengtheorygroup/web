---
title: Teaching

# Listing view
view: compact

# Optional header image (relative to `assets/media/` folder).
banner:
  caption: ''
  image: ''
---

## [<li>Computational Material Science (MSE1701)](mse1701/)

## [<li>Quantum Dynamics Simulations](chem2504)

<!---*<span style="font-size:0.7em">click the course for details</span>*>
