---
title: Chem2504
summary: Here we describe how to add a page to your site.
date: "2022-08-08T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
header:
  caption: ""
  image: ""
---



### Time

Tue 3:00 pm - 5:35 pm 

### Prerequisite	

One of the following courses: computational chemistry, computational physics, computational material science, quantum mechanicsm, and structural chemistry.

### Required coding language

python or C or Julia or Matlab or Fortran (at least you know one language)

### Grading policy

100% Homework

### Office hour

Friday 3:00pm - 4:00pm 

### Homework

* **week 1:** review basic concepts of quantum mechanics

* **week 2:** time-dependent perturbation theory I [hw1](CHEM2504-HW1.pdf)

* **week 3:** time-dependent perturbation theory II [hw2](CHEM2504-HW2.pdf)

* **week 4:** time-dependent perturbation theory III [hw3](CHEM2504-HW3.pdf)

* **week 5:** a general description of light-matter inateraction [hw4](CHEM2504-HW4.pdf)

* **week 6:** introduction to the density matrix representation [hw5](CHEM2504-HW5.pdf)

* **week 7:** linear response [hw6](CHEM2504-HW6.pdf)

* **week 8:** non-linear response

* **week 9:** adiabatic approximation and geometric phase [hw7](CHEM2504-HW7.pdf)

* **week 10:** introduction of density functional theory (DFT)

* **week 11:** *ab initio* molecular dynamics

* **week 12:** beyond BOMD: Ehrenfest molecular dynamics

* **week 13:** beyond BOMD: surface hopping and multiple spawning method [hw8](CHEM2504-HW8.pdf)
