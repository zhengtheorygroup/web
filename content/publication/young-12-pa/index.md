---
title: "First-Principles Calculation of the Bulk Photovoltaic Effect in Bismuth Ferrite"
date: 2012-12-01
publishDate: 2022-08-10T15:06:08.510597Z
authors: ["Steve M. Young", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review Letters*"
doi: "10.1103/PhysRevLett.109.236601"
---

