---
title: "Theoretical Investigation of 2D Conductive Microporous Coordination Polymers as Li-S Battery Cathode with Ultrahigh Energy Density"
date: 2018-09-01
publishDate: 2022-08-10T15:06:08.494081Z
authors: ["Guoping Gao", "Fan Zheng", "Feng Pan", "Lin-Wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Advanced Energy Materials*"
doi: "10.1002/aenm.201801823"
---

