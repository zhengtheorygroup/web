---
title: "Rashba Spin–Orbit Coupling Enhanced Carrier Lifetime in CH textsubscript3 NH textsubscript3 PbI textsubscript3"
date: 2015-12-01
publishDate: 2022-08-10T15:06:08.516879Z
authors: ["Fan Zheng", "Liang Z. Tan", "Shi Liu", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Nano Letters*"
doi: "10.1021/acs.nanolett.5b01854"
---

