---
title: "Material Innovation in Advancing Organometal Halide Perovskite Functionality"
date: 2015-12-01
publishDate: 2022-08-10T15:06:08.515749Z
authors: ["Fan Zheng", "Diomedes Saldana-Greco", "Shi Liu", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry Letters*"
doi: "10.1021/acs.jpclett.5b01830"
---

