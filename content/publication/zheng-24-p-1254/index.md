---
title: "Carrier Transport in 2D Hybrid Organic-Inorganic Perovskites: The Role of Spacer Molecules"
date: 2024-01-26
publishDate: 2024-01-26T15:06:08.523805Z
authors: ["Caihong Zheng", "Fan Zheng"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "J. Phys. Chem. Lett."
doi: "10.1021/acs.jpclett.3c03357"
---

