---
title: "Photoferroelectric and Photopiezoelectric Properties of Organometal Halide Perovskites"
date: 2016-04-01
publishDate: 2022-08-10T15:06:08.499014Z
authors: ["Shi Liu", "Fan Zheng", "Ilya Grinberg", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry Letters*"
doi: "10.1021/acs.jpclett.6b00527"
---

