---
title: "Prediction of a Linear Spin Bulk Photovoltaic Effect in Antiferromagnets"
date: 2013-01-01
publishDate: 2022-08-10T15:06:08.511625Z
authors: ["Steve M. Young", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review Letters*"
doi: "10.1103/PhysRevLett.110.057201"
---

