---
title: "Multiple k-Point Nonadiabatic Molecular Dynamics for Ultrafast Excitations in Periodic Systems: The Example of Photoexcited Silicon"
date: 2023-09-23
publishDate: 2023-09-23T15:06:08.523805Z
authors: ["Fan Zheng", "Lin-wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "Phys. Rev. Lett."
doi: "10.1103/PhysRevLett.131.156302"
---

