---
title: "Enhancing Ferroelectric Photovoltaic Effect by Polar Order Engineering"
date: 2018-07-01
publishDate: 2022-08-10T15:06:08.509509Z
authors: ["Lu You", "Fan Zheng", "Liang Fang", "Yang Zhou", "Liang Z. Tan", "Zeyu Zhang", "Guohong Ma", "Daniel Schmidt", "Andrivo Rusydi", "Le Wang", "Lei Chang", "Andrew M. Rappe", "Junling Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Science Advances*"
doi: "10.1126/sciadv.aat3438"
---

