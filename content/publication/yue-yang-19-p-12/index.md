---
title: "Ab Initio Investigation of Charge Trapping Across the Crystalline-Si–Amorphous-SiO2 Interface"
date: 2019-01-01
publishDate: 2022-08-10T15:06:08.513682Z
authors: ["Liu Yue-Yang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: ""
---

