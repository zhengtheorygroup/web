---
title: "Intermolecular Interactions in Hybrid Perovskites Understood from a Combined Density Functional Theory and Effective Hamiltonian Approach"
date: 2017-04-01
publishDate: 2022-08-10T15:06:08.504607Z
authors: ["Liang Z. Tan", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*ACS Energy Letters*"
doi: "10.1021/acsenergylett.7b00159"
---

