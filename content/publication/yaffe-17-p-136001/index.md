---
title: "Local Polar Fluctuations in Lead Halide Perovskite Crystals"
date: 2017-03-01
publishDate: 2022-08-10T15:06:08.507881Z
authors: ["Omer Yaffe", "Yinsheng Guo", "Liang Z. Tan", "David A. Egger", "Trevor Hull", "Constantinos C. Stoumpos", "Fan Zheng", "Tony F. Heinz", "Leeor Kronik", "Mercouri G. Kanatzidis", "Jonathan S. Owen", "Andrew M. Rappe", "Marcos A. Pimenta", "Louis E. Brus"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review Letters*"
doi: "10.1103/PhysRevLett.118.136001"
---

