---
title: "First-Principles Materials Design of High-Performing Bulk Photovoltaics with the Li Nb O 3 Structure"
date: 2015-11-01
publishDate: 2022-08-10T15:06:08.512648Z
authors: ["Steve M. Young", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review Applied*"
doi: "10.1103/PhysRevApplied.4.054004"
---

