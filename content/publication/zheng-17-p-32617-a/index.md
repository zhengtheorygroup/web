---
title: "Effects of the C-Si/a-SiO textsubscript2 Interfacial Atomic Structure on Its Band Alignment: An emphAb Initio Study"
date: 2017-01-01
publishDate: 2022-08-10T15:06:08.520133Z
authors: ["Fan Zheng", "Hieu H. Pham", "Lin-Wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Chemistry Chemical Physics*"
doi: "10.1039/C7CP05879A"
---

