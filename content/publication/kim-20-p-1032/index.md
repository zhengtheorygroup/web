---
title: "Selective CO2 Electrocatalysis at the Pseudocapacitive Nanoparticle/Ordered-Ligand Interlayer"
date: 2020-12-01
publishDate: 2022-08-10T15:06:08.496953Z
authors: ["Dohyung Kim", "Sunmoon Yu", "Fan Zheng", "Inwhan Roh", "Yifan Li", "Sheena Louisia", "Zhiyuan Qi", "Gabor A. Somorjai", "Heinz Frei", "Lin-Wang Wang", "Peidong Yang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Nature Energy*"
doi: "10.1038/s41560-020-00730-4"
---

