---
title: "Giant Bulk Photovoltaic Effect in Vinylene-Linked Hybrid Heterocyclic Polymer"
date: 2017-03-01
publishDate: 2022-08-10T15:06:08.500051Z
authors: ["Shi Liu", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry C*"
doi: "10.1021/acs.jpcc.7b00374"
---

