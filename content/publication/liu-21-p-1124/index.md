---
title: "Investigation and Mitigation of Degradation Mechanisms in Cu2O Photoelectrodes for CO2 Reduction to Ethylene"
date: 2021-12-01
publishDate: 2022-08-10T15:06:08.501076Z
authors: ["Guiji Liu", "Fan Zheng", "Junrui Li", "Guosong Zeng", "Yifan Ye", "David M. Larson", "Junko Yano", "Ethan J. Crumlin", "Joel W. Ager", "Lin-wang Wang", "Francesca M. Toma"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Nature Energy*"
doi: "10.1038/s41560-021-00927-1"
---

