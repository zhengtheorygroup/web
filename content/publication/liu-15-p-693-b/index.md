---
title: "Ferroelectric Domain Wall Induced Band Gap Reduction and Charge Separation in Organometal Halide Perovskites"
date: 2015-02-01
publishDate: 2022-08-10T15:06:08.497990Z
authors: ["Shi Liu", "Fan Zheng", "Nathan Z. Koocher", "Hiroyuki Takenaka", "Fenggong Wang", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry Letters*"
doi: "10.1021/jz502666j"
---

