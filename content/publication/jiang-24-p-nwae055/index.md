---
title: "Surface heterojunction based on n-type low dimensional perovskite film for highly efficient perovskite tandem solar cells"
date: 2024-02-13
publishDate: 2024-02-13T15:06:08.523805Z
authors: ["Xianyuan Jiang", "Qilin Zhou", "Yue Lu", "Hao Liang", "Wenzhuo Li", "Qi Wei", "Mengling Pan", "Xin Wen", "Xingzhi Wang", "Wei Zhou", "Danni Yu", "Hao Wang", "Ni Yin", "Hao Chen", "Hansheng Li", "Ting Pan", "Mingyu Ma", "Gaoqi Liu", "Wenjia Zhou", "Zhenhuang Su", "Qi Chen", "Fengjia Fan", "Fan Zheng", "Xingyu Gao", "Qingqing Ji", "Zhijun Ning"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "National Science Review"
doi: "10.1093/nsr/nwae055"
---

