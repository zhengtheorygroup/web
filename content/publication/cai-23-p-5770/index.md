---
title: "Promotion of the Nucleation of Ultrafine Ni-Rich Layered Oxide Primary Particles by an Atomic Layer-Deposited Thin Film for Enhanced Mechanical Stability"
date: 2023-06-14
publishDate: 2023-06-14T15:06:08.523805Z
authors: ["Xincan Cai", "Zihan Wang", "Yurui Xing", "Caihong Zheng", "Pu Yan", "Wenda Bao", "Tianye Xie", "Yuxiong Hu", "Yingdong Deng", "Yue Zhang", "Yifan Wu", "Shaoyu Yang", "Fan Zheng", "Hongti Zhang", "Zhu-Jun Wang", "Jin Xie"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "Nano Lett."
doi: "10.1021/acs.nanolett.3c01648"
---

