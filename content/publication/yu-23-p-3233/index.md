---
title: "Electron-withdrawing organic ligand for high efficiency all-perovskite tandem solar cells"
date: 2024-01-12
publishDate: 2024-01-12T15:06:08.523805Z
authors: ["Danni Yu", "Mengling Pan", "Gaoqi Liu", "Xianyuan Jiang", "Xin Wen", "Wenzhuo Li", "Shaojie Chen", "Wenjia Zhou", "Hao Wang", "Yue Lu", "Mingyu Ma", "Zihao Zang", "Peihong Cheng", "Qingqing Ji", "Fan Zheng", "Zhijun Ning"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "Nat. Energy"
doi: "10.1038/s41560-023-01441-2"
---

