---
title: "Quantum Pressure and Chemical Bonding: Influence of Magnetic Fields on Electron Localization"
date: 2015-08-01
publishDate: 2022-08-10T15:06:08.505634Z
authors: ["Jianmin Tao", "Shi Liu", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review B*"
doi: "10.1103/PhysRevB.92.060401"
---

