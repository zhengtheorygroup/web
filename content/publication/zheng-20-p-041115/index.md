---
title: "Exploring Non-Adiabaticity to CO Reduction Reaction through Ab Initio Molecular Dynamics Simulation"
date: 2020-04-01
publishDate: 2022-08-10T15:06:08.523805Z
authors: ["Fan Zheng", "Lin-wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*APL Materials*"
doi: "10.1063/5.0002318"
---

