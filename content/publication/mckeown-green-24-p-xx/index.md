---
title: "Millimeter-scale Exfoliation of hBN with Tunable Flake Thickness for Scalable Encapsulation"
date: 2024-02-19
publishDate: 2024-02-19T15:06:08.523805Z
authors: ["Amy S. McKeown-Green", "Helen J. Zeng", "Ashley P. Saunders", "Jiayi Li", "Jenny Hu", "Jiaojian Shi", "Yuejun Shen", "Feng Pan", "Jennifer A. Dionne", "Tony F. Heinz", "Stephen Wu", "Fan Zheng", "Fang Liu"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "ACS Applied Nano Materials"
doi: ""
---

