---
title: "Frequency-Dependent Dielectric Function of Semiconductors with Application to Physisorption"
date: 2017-01-01
publishDate: 2022-08-10T15:06:08.519075Z
authors: ["Fan Zheng", "Jianmin Tao", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review B*"
doi: "10.1103/PhysRevB.95.035203"
---

