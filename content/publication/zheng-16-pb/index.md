---
title: "Substantial Optical Dielectric Enhancement by Volume Compression in LiAsSe 2"
date: 2016-05-01
publishDate: 2022-08-10T15:06:08.517915Z
authors: ["Fan Zheng", "John A. Brehm", "Steve M. Young", "Youngkuk Kim", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review B*"
doi: "10.1103/PhysRevB.93.195210"
---

