---
title: "Shift Current Bulk Photovoltaic Effect in Polar Materials— Hybrid and Oxide Perovskites and Beyond"
date: 2016-11-01
publishDate: 2022-08-10T15:06:08.503565Z
authors: ["Liang Z Tan", "Fan Zheng", "Steve M Young", "Fenggong Wang", "Shi Liu", "Andrew M Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*npj Computational Materials*"
tags: ["review", "shift current"]
doi: "10.1038/npjcompumats.2016.26"
---

