---
title: "First-Principles Calculation of the Bulk Photovoltaic Effect in the Polar Compounds LiAsS2, LiAsSe2, and NaAsSe2"
date: 2014-11-01
publishDate: 2022-08-10T15:06:08.492378Z
authors: ["John A. Brehm", "Steve M. Young", "Fan Zheng", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Chemical Physics*"
doi: "10.1063/1.4901433"
---

