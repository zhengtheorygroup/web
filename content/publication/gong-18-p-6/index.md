---
title: "Phonon Influence on Bulk Photovoltaic Effect in the Ferroelectric Semiconductor GeTe"
date: 2018-01-01
publishDate: 2022-08-10T15:06:08.495638Z
authors: ["Shi-Jing Gong", "Fan Zheng", "Andrew M Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*PHYSICAL REVIEW LETTERS*"
---

