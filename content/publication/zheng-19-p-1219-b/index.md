---
title: "Large Polaron Formation and Its Effect on Electron Transport in Hybrid Perovskites"
date: 2019-01-01
publishDate: 2022-08-10T15:06:08.521173Z
authors: ["Fan Zheng", "Lin-wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Energy & Environmental Science*"
doi: "10.1039/C8EE03369B"
---

