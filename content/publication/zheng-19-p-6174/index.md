---
title: "Ultrafast Hot Carrier Injection in Au/GaN: The Role of Band Bending and the Interface Band Structure"
date: 2019-10-01
publishDate: 2022-08-10T15:06:08.522222Z
authors: ["Fan Zheng", "Lin-Wang Wang"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry Letters*"
doi: "10.1021/acs.jpclett.9b02402"
---

