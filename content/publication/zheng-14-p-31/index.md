---
title: "First-Principles Calculation of the Bulk Photovoltaic Effect in CH3NH3PbI3 and CH3NH3PbI3– x Cl x"
date: 2014-01-01
publishDate: 2022-08-10T15:06:08.514722Z
authors: ["Fan Zheng", "Hiroyuki Takenaka", "Fenggong Wang", "Nathan Z Koocher", "Andrew M Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*The Journal of Physical Chemistry Letters*"
---

