---
title: "Screened van Der Waals Correction to Density Functional Theory for Solids"
date: 2017-07-01
publishDate: 2022-08-10T15:06:08.506769Z
authors: ["Jianmin Tao", "Fan Zheng", "Julian Gebhardt", "John P. Perdew", "Andrew M. Rappe"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review Materials*"
doi: "10.1103/PhysRevMaterials.1.020802"
---

