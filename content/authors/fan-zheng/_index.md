---
# Display name
title: Fan Zheng

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Professor

# Organizations/Affiliations
organizations:
  - name: ShanghaiTech University
    url: ''

# Short bio (displayed in user profile at end of posts)
bio: My research interests include computational material sciences, first-principle calculations, and excited states

interests:
  - first-principle photophysical phenomena
  - excited properties in condensed matters
  - large-scale simulation
  - catalysis and solvent system

education:
  courses:
    - course: Postdoc fellow Chemical Science (JCAP)
      institution: Lawrence Berkeley National Lab
      year: 2017-2021
    - course: PhD in chemistry
      institution: University of Pennsylvania
      year: 2010-2016
    - course: BSc in chemical physics
      institution: University of Science and Technology of China (USTC)
      year: 2006-2010

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:zhengfan@shanghaitech.edu.cn'
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.com/citations?user=KwRlfz0AAAAJ&hl=en
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/zhengfann
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Principal Investigators
---

Fan Zheng is a professor of material science at the ShanghaiTech University. His research interests include developing ab initio methods to simulate carrier dynamics in materials. By applying the first-priniciple’s computation methods, he is interested in illustrating the ultrafast process of the excited carriers’ motion in various materials/devices, including bulk materials, interfaces, chemical reactions, and nano-materials. Understanding these processes at an atomic level will help to design more efficient materials or devices. 
