---
title: Research Interest

# Listing view
view: compact

# Optional header image (relative to `assets/media/` folder).
banner:
  caption: ''
  image: ''
---

## <li>Real-time simulation of excited carrier dynamics

<img src="research1.png" width=320>

How do the excited electrons and holes evolve as a function of time? This question is important to the improvement of performance of energy materials (e.g. solar cells). We are interested in using real-time non-adiabatic molecular dynamics (NAMD) to directly simulate the dynamics of an electron (or a hole) after the light excitation. These simulations can help us discover what is important to the carrier dynamics such as interfacial structures, atomic vibrations, or chemical bondings, which is vital to design new high-performance energy materials. [more]()

## <li>Large polaron formation and its dynamics in Halide perovskite

<img src="research2.png" width=600>

Large polaron is a consequence of an electron interacting with phonons. In many cases, large polaron tends to drag the excited carriers and reduce light-generated photocurrent (e.g. in solar cells). However, we believe that in hybrid perovskite, the formation of large polaron will protect the carriers avoiding scatterings from defects, dynamical disorder, and other carriers, introducing long carrier lifetime high power-conversion efficiency in these materials. A multi-scale simulation will tell us more details. [more]()

## <li>Beyond Born-Oppenheimer: time-dependent DFT simulations

<img src="research3.png" width=400>

Atomic structures shape the evolution of excited carriers, meanwhile, these "high-energy" carriers pry the atoms although the mass of the carrier is less than 1/1000 of an atom. We are interested in using time-dependent density functional theory as a tool to investigate excited electronic structure induced structural change. For example, plasmon catalyzed chemical reactions, light induced phase transitions, photo-corrosions, non-adiabaticity in chemical reactions, etc. 