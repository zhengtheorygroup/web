---
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget: hero # See https://wowchemy.com/docs/page-builder/
headless: true # This file represents a page section.
weight: 5 # Order that this section will appear.
title: |

  Computational Material Science group
  
  @ShanghaiTech
hero_media: welcome.jpg
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '1'
  # Add custom styles
  css_style:
  css_class:
---

<br>

Welcome to the **CMS Group** @ShanghaiTech! We are focusing on using theoretical models and supercomputers to understand how light interacts with matters. By exploring the photophysical phenomena at an atomic level, we hope to find out a better energy source. 
